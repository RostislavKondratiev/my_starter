import { NgModule } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InputErrorsComponent } from './components/input-errors/input-errors.component';
import { SmartInputComponent } from './components/smart-input/smart-input.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    InputErrorsComponent,
    SmartInputComponent
  ],
  providers: [],
  exports: [
    HeaderComponent,
    FooterComponent,
    InputErrorsComponent,
    SmartInputComponent
  ]
})
export class SharedModule {
}
