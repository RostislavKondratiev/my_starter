
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rn-smart-input',
  templateUrl: './smart-input.component.html',
  styleUrls: ['./smart-input.component.scss']
})
export class SmartInputComponent implements OnInit {
  @Input() public control;
  @Input() public placeholder;
  @Input() public type;
  public ngOnInit() {
  }
}
